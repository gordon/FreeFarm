**DEPRECATED**

An open source farming game inspired by such games as "Stardew Valley" and "Harvest Master".

Version 0.0.2

To run this game you should install löve2d and zip (unzip):

https://love2d.org/

**Building the game:**

*Currently the buildscript runs only in Unix-like systems like Linux, MacOS and BSD:*

Run build.sh or open Terminal in the game directory and type "make".

This will create an executable for the game.

*Using Windows:*

Put the whole game folder in a zip file like *FreeFarm.zip* and rename it in *FreeFarm.love*.
