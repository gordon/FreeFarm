function love.conf(t)
    t.window.width = 0
    t.window.height = 0
    t.window.title = "Free Farm"
    t.window.fullscreen = true
    t.window.fullscreentype = "desktop"
    t.window.borderless = false
    t.version = "11.2"
end
