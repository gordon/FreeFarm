local icon = love.image.newImageData("Sprites/icon.png")
local exitdialog = love.graphics.newImage("Sprites/Menu/exitdialog.png")
local dialog_x = 0
local dialog_y = 0
local starttime = 0
local time = 0
local hours = 6
local minutes = 0
local music = love.audio.newSource("Sounds/music.ogg", "stream")
local steps = love.audio.newSource("Sounds/steps.ogg", "static")
love.window.setIcon(icon)
love.graphics.setBackgroundColor(255, 255, 255)
local Object = require "classic"
local Player = Object:extend()
local Cursor = Object:extend()
local width = love.graphics.getWidth()
local height = love.graphics.getHeight()
local FramesUP = {}
local FramesDOWN = {}
local FramesLEFT = {}
local FramesRIGHT = {}
local frames = {}
local activeFrame
local currentFrame = 1
local elapsedTime = 0
local offset_x = 0
local offset_y = 0
local center_x
local center_y
local tile = {}
local collisiontiles = {}
local exitgame = false
for i=1,24 do
  tile[i] = love.graphics.newImage("Sprites/Tiles/tile"..i..".png")
end
local map = {
        {21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21},
        {22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1},
        {1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 13, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 4, 17, 19, 19, 18, 4, 4, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 4, 4, 3, 3, 3, 17, 19, 19, 18, 3, 3, 3, 4, 4, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 3, 3, 3, 3, 3, 3, 3, 17, 19, 19, 18, 3, 3, 3, 3, 3, 3, 4, 4, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 8, 5, 5},
        {1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 14, 1, 1, 1, 1, 1, 1, 1, 1, 8, 3, 3, 3, 3, 3, 3, 3, 3, 17, 19, 19, 18, 3, 3, 3, 3, 3, 3, 3, 3, 3, 9, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 4, 4, 11, 1, 1},
        {1, 13, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 8, 3, 3, 3, 3, 3, 3, 3, 3, 3, 15, 20, 20, 16, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 4, 5, 5, 5, 11, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 3, 5, 11, 1, 1, 1, 1, 1, 1, 13},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 3, 3, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 8, 4, 3, 3, 3, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 8, 4, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 9, 1, 1, 1, 1, 8, 3, 3, 5, 5, 5, 3, 7, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 8, 3, 3, 11, 1, 1, 1, 6, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 13, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 8, 4, 3, 3, 11, 1, 1, 1, 1, 6, 3, 9, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 3, 5, 11, 1, 1, 1, 1, 1, 10, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 11, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 12, 1, 1, 12, 1, 1, 1, 1, 1, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 11, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5, 3, 3, 3, 5, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 12, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1},
        {13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 3, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 6, 3, 3, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 3, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
      }

map_w = #map[1]
map_h = #map
map_x = 0
map_y = 0
map_display_buffer = 2
tile_w = 30
tile_h = 30
map_display_w = width/tile_w
map_display_h = height/tile_h

local collisiontiles = {[3]=true,[4]=true,[5]=true,[6]=true,[7]=true,[8]=true,[9]=true,[10]=true,[11]=true, [21]=true, [22]=true, [23]=true, [24]=true}

function Player.new(self)
  self.img = love.graphics.newImage("Sprites/hero.png")
  self.x = 0
  self.y = 0
  self.direction = "down"
  self.speed = 150
  self.width = 21
  self.height = 15
end
function Cursor.new(self)
  self.x = 0
  self.y = 0
  self.img = love.graphics.newImage("Sprites/cursor.png")
end
local player = Player()
local cursor = Cursor()

function get_tile_at(x,y)
  local iy = math.floor(y / tile_h + 1)
  if map[iy] then
    return map[iy][math.floor(x / tile_w + 1)]
  else
    return nil
  end
end

function object_touches(x,y, w,h, material)
  local x, y, w, h = math.floor(x), math.floor(y), math.floor(w), math.floor(h)
  -- general grid through object
  for yi = y, y+h, tile_h do
    for xi = x, x+w, tile_w do
      if material[get_tile_at(xi,yi)] then
        return true
      end
    end
  end

  -- Special case right border
  for yi = y, y+h, tile_h do
    if material[get_tile_at(x+w,yi)] then
      return true
    end
  end

  -- left corners
  if material[get_tile_at(x+w, y)] or material[get_tile_at(x, y+h)] or material[get_tile_at(x+w, y+h)] then
    return true
  else
    return false
  end
end

function draw_map(offset_x, offset_y)
  for y = offset_y, offset_y + (map_h-1) * tile_h, tile_h do
    for x = offset_x, offset_x + (map_w-1) * tile_w, tile_w do
      if (x + tile_w > 0) and (y + tile_h > 0) and (x < width) and (y < height) then
        local iy = (y-offset_y) / tile_h
        local ix = (x-offset_x) / tile_w
        love.graphics.draw(tile[map[iy+1][ix+1]], x, y)
      end
    end
  end
end

function love.load()
  starttime = love.timer.getTime()
  dialog_x = (width-exitdialog:getWidth())/2
  dialog_y = (height-exitdialog:getHeight())/2
  love.mouse.setVisible(false)
  FramesDOWN[1] = love.graphics.newQuad(1,1,16,30,player.img:getDimensions())
  FramesDOWN[2] = love.graphics.newQuad(19,1,16,30,player.img:getDimensions())
  FramesDOWN[3] = love.graphics.newQuad(37,1,16,30,player.img:getDimensions())
  FramesDOWN[4] = love.graphics.newQuad(55,1,16,30,player.img:getDimensions())
  FramesDOWN[5] = love.graphics.newQuad(73,1,16,30,player.img:getDimensions())
  FramesUP[1] = love.graphics.newQuad(1,33,16,30,player.img:getDimensions())
  FramesUP[2] = love.graphics.newQuad(19,33,16,30,player.img:getDimensions())
  FramesUP[3] = love.graphics.newQuad(37,33,16,30,player.img:getDimensions())
  FramesUP[4] = love.graphics.newQuad(55,33,16,30,player.img:getDimensions())
  FramesUP[5] = love.graphics.newQuad(73,33,16,30,player.img:getDimensions())
  FramesLEFT[1] = love.graphics.newQuad(1,65,16,30,player.img:getDimensions())
  FramesLEFT[2] = love.graphics.newQuad(19,65,16,30,player.img:getDimensions())
  FramesLEFT[3] = love.graphics.newQuad(37,65,16,30,player.img:getDimensions())
  FramesLEFT[4] = love.graphics.newQuad(55,65,16,30,player.img:getDimensions())
  FramesLEFT[5] = love.graphics.newQuad(73,65,16,30,player.img:getDimensions())
  FramesRIGHT[1] = love.graphics.newQuad(1,97,16,30,player.img:getDimensions())
  FramesRIGHT[2] = love.graphics.newQuad(19,97,16,30,player.img:getDimensions())
  FramesRIGHT[3] = love.graphics.newQuad(37,97,16,30,player.img:getDimensions())
  FramesRIGHT[4] = love.graphics.newQuad(55,97,16,30,player.img:getDimensions())
  FramesRIGHT[5] = love.graphics.newQuad(73,97,16,30,player.img:getDimensions())
  frames = FramesDOWN
  activeFrame = frames[currentFrame]
  player.x = 400
  player.y = 300
  center_x = player.x
  center_y = player.y
end

function love.draw()
  local graphical_player_pos_x = width / 2
  local graphical_player_pos_y = height / 2
  offset_x = math.min(0, math.max(graphical_player_pos_x - player.x, width - map_w * tile_w))
  offset_y = math.min(0, math.max(graphical_player_pos_y - player.y, height - map_h * tile_h))

  draw_map(offset_x, offset_y)
  activeFrame = frames[currentFrame]
  love.graphics.draw(player.img, activeFrame, offset_x + player.x, offset_y + player.y - player.height*2, 0, 1.5, 1.5)
  cursor.x, cursor.y = love.mouse.getPosition()
  love.graphics.setColor(0,0,0,255)
  love.graphics.setNewFont(12)
  love.graphics.print("FPS: "..love.timer.getFPS(), 5, 10)
  love.graphics.print("X: " .. math.floor(player.x), 5, 25)
  love.graphics.print("Y: " .. math.floor(player.y), 5, 40)
  love.graphics.setNewFont(20)
  love.graphics.setColor(255,255,255,255)
  love.graphics.rectangle("fill", width-135, 5, 130, 30)
  love.graphics.setColor(0,0,0,255)
  if minutes < 10 and hours < 10 then
    love.graphics.print("Time: 0" .. hours .. ":0" .. minutes, width-130, 10)
  elseif minutes >= 10 and hours < 10 then
    love.graphics.print("Time: 0" .. hours .. ":" .. minutes, width-130, 10)
  elseif minutes < 10 and hours >= 10 then
    love.graphics.print("Time: " .. hours .. ":0" .. minutes, width-130, 10)
  else
    love.graphics.print("Time: " .. hours .. ":" .. minutes, width-130, 10)
  end
  --love.graphics.print("Width: " .. width, 5, 55)
  --love.graphics.print("Height: " .. height, 5, 70)
  love.graphics.setColor(255,255,255,255)
  if exitgame then
    love.graphics.draw(exitdialog, dialog_x, dialog_y)
  end
  love.graphics.draw(cursor.img, cursor.x, cursor.y, 0, 0.15, 0.15)
  --love.graphics.rectangle("line", player.x+offset_x, player.y+offset_y, player.width, player.height)

end

 function changeFrame()
  if elapsedTime > 0.1 then
    if currentFrame < 5 then
      currentFrame = currentFrame + 1
    else
      currentFrame = 1
    end
    elapsedTime = 0
  end
end


function love.update(dt)
    time = love.timer.getTime()
    if (time - starttime) >= 1 then
        minutes = minutes + 1
        if minutes == 60 then
            minutes = 0
            hours = hours + 1
            if hours == 24 then
                hours = 0
            end
        end
        starttime = love.timer.getTime()
    end
    cursor.x, cursor.y = love.mouse.getPosition()
    local mouse_x = cursor.x + (cursor.img:getWidth()*0.15/3) - offset_x
    local mouse_y = cursor.y + (cursor.img:getHeight()*0.15/3) - offset_y
    music:play()
    elapsedTime = elapsedTime+dt
    if exitgame == true then
        if love.mouse.isDown(1) then
            if cursor.x > dialog_x and cursor.x < dialog_x+(exitdialog:getWidth()/2) and
                cursor.y > dialog_y and cursor.y < dialog_y+exitdialog:getHeight() then
                exitgame = false
            elseif
                cursor.x > dialog_x+(exitdialog:getWidth()/2) and cursor.x < dialog_x+exitdialog:getWidth() and
                cursor.y > dialog_y and cursor.y < dialog_y+exitdialog:getHeight() then
                love.event.quit()
            end
        end

    else
        local move_x = 0
        local move_y = 0

        -- Position move proposal
        if love.keyboard.isDown('w') then
            player.direction = "up"
            move_y = move_y - player.speed*dt
            steps:play()
        end
        if love.keyboard.isDown('s') then
            player.direction = "down"
            move_y = move_y + player.speed*dt
            steps:play()
        end
        if love.keyboard.isDown('a') then
            player.direction = "left"
            move_x = move_x - player.speed*dt
            steps:play()
        end
        if love.keyboard.isDown('d') then
            player.direction = "right"
            move_x = move_x + player.speed*dt
            steps:play()
        end

        --Farming

        if love.mouse.isDown(1) then
            local farming = false
            local player_x = player.x+(player.width/2)
            local player_y = player.y+(player.height/2)
	    local x_diff = math.abs(player_x-mouse_x)
	    local y_diff = math.abs(player_y-mouse_y)
            if (x_diff <= 1.5*tile_w) and (y_diff <= 1.5*tile_h) then
              if y_diff > x_diff then
                if mouse_y > player_y then
                    player.direction = "down"
                    frames = FramesDOWN
                elseif mouse_y < player_y then
                    player.direction = "up"
                    frames = FramesUP
                end
              else
	    	if mouse_x > player_x then
                    player.direction = "right"
                    frames = FramesRIGHT
                elseif mouse_x < player_x then
                    player.direction = "left"
                    frames = FramesLEFT
                else
                    player.direction = player.direction
                end
              end
              farming = true
            end

            if farming then
                local ix = math.ceil(mouse_x / tile_w)
                local iy = math.ceil(mouse_y / tile_h)
                local id = map[iy][ix]
                if (id == 1) or (id == 12) or (id == 13) then
                    map[iy][ix] = 2
                end
            end
        end

        -- Game control
        if love.keyboard.isDown('escape') then
            exitgame = true
        end


        -- Can we approve that new position?
        local oldx, oldy = player.x, player.y
        local newx = player.x + move_x
        if (newx >= 0) and (newx < map_w * tile_w - player.width) and (not object_touches(newx, player.y, player.width, player.height, collisiontiles)) then
            player.x = newx
        end

        local newy = player.y + move_y
        if (newy >= 0) and (newy < map_h * tile_h - player.height) and (not object_touches(player.x, newy, player.width, player.height, collisiontiles)) then
            player.y = newy
        end

        if object_touches(player.x, player.y, player.width, player.height, collisiontiles) then
            player.x, player.y = oldx, oldy
        end

        -- Animations
        if move_x > 0 then
	       frames = FramesRIGHT
	       changeFrame()
        elseif move_x < 0 then
	       frames = FramesLEFT
	       changeFrame()
        elseif move_y > 0 then
	       frames = FramesDOWN
	       changeFrame()
        elseif move_y < 0 then
	       frames = FramesUP
	       changeFrame()
        else
	       currentFrame = 1
        end
    end
end
