all: clean
	zip -9 -r FreeFarm.love .
	cat `which love` FreeFarm.love > FreeFarm
	chmod +x FreeFarm
run: all
	love FreeFarm.love
clean:
	rm -f FreeFarm FreeFarm.love
